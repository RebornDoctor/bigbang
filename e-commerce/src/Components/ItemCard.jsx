import React from 'react'
import "./ItemCard.scss"
export default function ItemCard({ image, itemName, description, caption }) {
    return (
        <div className="ItemCard">
            <div className="img">
                <img src={image} className="img-fluid" alt="item" />
            </div>
            <div className="caption">
                {caption}
            </div>
            <div className="name">
                {itemName}
            </div>
            <div className="description mt-3">
                {description}
            </div>
        </div>
    )
}
