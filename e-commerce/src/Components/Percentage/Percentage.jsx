import React from 'react'
import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./Percentage.scss";

export default function Percentage({ q, Qsold }) {
    const percentage = Qsold;
    const maxV = q;
    const progressBarStyle = buildStyles({
        pathColor: "#7f1aa8",
        trailColor: "#e2d0e7"
    })

    return (
        <div className="CircleProgress">
            <CircularProgressbarWithChildren value={percentage} maxValue={maxV} styles={progressBarStyle} strokeWidth={4.5} >
                <div className="caption shadow bg-body">
                    <div className="one">{percentage}</div>
                    <div className="second">SOLD</div>
                    <div className="third">OUT OF</div>
                    <div className="fourth">{maxV}</div>
                </div>

            </CircularProgressbarWithChildren>
        </div>
    )
}
