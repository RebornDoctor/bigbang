/* eslint-disable eqeqeq */
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const getItemsAsync = createAsyncThunk(
  "items/getItemsAsync",
  async () => {
    const response = await fetch(
      "https://wawinner.its.ae/dev/public/api/v1/front-end/campaign"
    );
    if (response.ok) {
      const items = await response.json();
      console.log({ items });
      return { items };
    }
  }
);
const initialState = {
  items: [],
};

export const itemsSlice = createSlice({
  name: "items",
  initialState,
  reducers: {},
  extraReducers: {
    [getItemsAsync.fulfilled]: (state, action) => {
      state.items = action.payload.items.data;
    },
  },
});

export default itemsSlice.reducer;
