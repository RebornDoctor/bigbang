import { configureStore } from "@reduxjs/toolkit";
import itemsReducer from "./items";
export const store = configureStore({
  reducer: {
    item: itemsReducer,
  },
});
