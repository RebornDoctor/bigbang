import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getItemsAsync } from "../Redux/items"
import "./Home.scss";
import ItemCard from '../Components/ItemCard';
import Percentage from '../Components/Percentage/Percentage';
export default function Home() {
    const { items } = useSelector(state => state.item);
    const dispatch = useDispatch();
    const [cartCounter, setcartCounter] = useState(1);
    const [randomItem, setrandomItem] = useState(false);

    useEffect(() => {
        dispatch(getItemsAsync())
    }, [dispatch])


    useEffect(() => {
        // eslint-disable-next-line eqeqeq
        if (items.length != 0)
            setrandomItem(items[Math.floor(Math.random() * items.length)])
    }, [items])
    // eslint-disable-next-line eqeqeq
    if (randomItem) {

        var Q = randomItem.product_quantity;
        var Qsold = randomItem.quantity_sold;
        var price = randomItem.product_price
        var itemImage = randomItem.product_id.image;
        var itemName = randomItem.product_id.name
        var itemDescription = randomItem.product_id.description;
        var prizeImage = randomItem.prize_id.image;
        var prizeName = randomItem.prize_id.name;
        var prizeDescription = randomItem.prize_id.description;
    }
    return (
        <div className="container" >
            <div className="Items-Container shadow p-3 mb-4 bg-body">
                <div className="row g-0">
                    <div className="col-4">

                        <div className="price-container">
                            <div className="price-3d mt-3">

                            </div>
                            <div className="price mt-3">
                                AED {price}
                            </div>
                        </div>

                    </div>
                    <div className="col-4 top-middle-col">
                        <Percentage q={Q} Qsold={Qsold} />
                    </div>

                    <div className="col-4">
                        <div className="res-actions">
                            <button className="Like" >
                                <i className="fas fa-heart"></i>
                            </button>
                            <button className="cart" >
                                <i className="fas fa-cart-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div className="row g-0 mt-5">
                    <div className="col-md-6 mb-2">
                        <div className="Item-Position ">
                            <ItemCard image={itemImage} itemName={itemName} description={itemDescription} caption={"Buy a"} />
                        </div>
                        <div className="mt-4 mb-4 cart-res">
                            <div className="actions-cart2"><button className="rounded-circle" onClick={() => setcartCounter(cartCounter + 1)}>+</button> <div className="counter">{cartCounter}</div><button className="rounded-circle" onClick={() => setcartCounter(cartCounter - 1)}>-</button></div>
                            <div><button className="rounded-pill show">Show your cart</button></div>
                        </div>
                    </div>

                    <div className="col-md-6 mb-2">
                        <div className="row g-0">
                            <div className="col-11">
                                <div className="Item-Positions">
                                    <ItemCard image={prizeImage} itemName={prizeName} description={prizeDescription} caption={"Get a chance to win:"} />
                                </div>
                            </div>
                            <div className="col-1">
                                <div className="actions">
                                    <button className="Like" >
                                        <i className="fas fa-heart"></i>
                                    </button>
                                    <button className="cart" >
                                        <i className="fas fa-cart-plus"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                        {/*  <div className="half-circle">
                            <div className="qunatity">
                                1
                            </div>
                          </div> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
