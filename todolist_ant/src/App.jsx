import './App.scss';
import TodoItem from './components/TodoItem/TodoItem';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import React, { useState } from 'react'
import AddTodo from "./components/TodoModal/TodoModal"
function App() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { todos } = useSelector(state => state.todo)
  console.log(todos)

  const showModal = () => {
    setIsModalVisible(true);
  };
  return (
    <div>
      <div className="todo-header">Todo List</div>
      <div className="todo-container ">
        <div className="todoItems-container">
          {todos.map((item) => (
            <TodoItem key={item.id} id={item.id} title={item.title} date={item.date} canceled={item.canceled} completed={item.completed} />
          ))}

        </div>
        <div className="add-todoItem">
          <Button type="default" className="add-btn" shape="circle" size="large" icon={<PlusOutlined />} onClick={showModal} />
          <AddTodo isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible} />
        </div>
      </div>
    </div>
  );
}

export default App;
