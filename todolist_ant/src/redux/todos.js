/* eslint-disable eqeqeq */
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todos: [],
};

export const todoSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      const todo = action.payload;
      state.todos.push({
        id: Date.now(),
        date: todo.date,
        title: todo.title,
        completed: false,
        canceled: false,
      });
    },
    deleteTodo: (state, action) => {
      const todoID = action.payload;
      state.todos = state.todos.filter((item) => item.id !== todoID);
    },
    editTodo: (state, action) => {
      const ObjIndex = state.todos.findIndex(
        (obj) => obj.id == action.payload.id
      );
      state.todos[ObjIndex].title = action.payload.title;
    },

    completeTodo: (state, action) => {
      const ObjIndex = state.todos.findIndex(
        (obj) => obj.id == action.payload.id
      );
      state.todos[ObjIndex].completed = true;
    },

    cancelTodo: (state, action) => {
      const ObjIndex = state.todos.findIndex(
        (obj) => obj.id == action.payload.id
      );
      state.todos[ObjIndex].canceled = true;
    },
  },
});

export const { addTodo, deleteTodo, editTodo, completeTodo, cancelTodo } =
  todoSlice.actions;
export default todoSlice.reducer;
