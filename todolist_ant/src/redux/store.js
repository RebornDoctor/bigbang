import { configureStore } from "@reduxjs/toolkit";
import todoReducer from "./todos.js";
export const store = configureStore({
  reducer: {
    todo: todoReducer,
  },
});
