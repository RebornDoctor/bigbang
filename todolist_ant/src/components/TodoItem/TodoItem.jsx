import React, { useState } from 'react'
import "./TodoItem.scss"
import { Button } from 'antd';
import { DeleteFilled, EditFilled, CloseOutlined, CheckOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { deleteTodo, completeTodo, cancelTodo } from "../../redux/todos"
import EditTodo from "../TodoModal/TodoModal"
export default function TodoItem({ id, title, date, canceled, completed }) {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const dispatch = useDispatch();

    const HandleDeleting = () => {
        dispatch(deleteTodo(id))
    }
    const HandleEditing = () => {
        setIsModalVisible(true)
    }

    const HandleCompleting = () => {
        dispatch(completeTodo({ id: id }))
    }
    const HandleCanceling = () => {
        dispatch(cancelTodo({ id: id }))
    }
    return (
        <div className={` ${canceled ? "todo-card-canceled" : "todo-card"} ${completed ? "todo-card-done" : "todo-card"} `}>
            <div className="title">
                <div className="date">{date}</div>
                <div className="name">{title}</div>
            </div>
            <div className="actions">
                <Button type="danger" className="delete" shape="circle" onClick={() => HandleDeleting()} icon={<DeleteFilled />} />
                <Button type="primary" className="edit" shape="circle" onClick={() => HandleEditing()} icon={<EditFilled />} />
                <EditTodo isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible} EditModal={true} id={id} />
                <Button type="danger" className="cancel" shape="circle" onClick={() => HandleCanceling()} icon={<CloseOutlined />} />
                <Button type="default" className="done" shape="circle" onClick={() => HandleCompleting()} icon={<CheckOutlined />} />
            </div>
        </div>
    )
}
