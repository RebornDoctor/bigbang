/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react'
import "./TodoModal.scss"
import { Modal, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { addTodo, editTodo } from "../../redux/todos";
import moment from 'moment';

export default function TodoModal({ setIsModalVisible, isModalVisible, EditModal, id }) {
    const [addTodoValue, setaddTodoValue] = useState("")
    const dispatch = useDispatch();

    const todo = useSelector(state => state.todo.todos.find((item) => item.id == id));

    useEffect(() => {
        if (EditModal) {
            setaddTodoValue(todo.title)
        }
    }, [EditModal, setaddTodoValue, todo])

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const HandleAddtodo = () => {
        if (addTodoValue === "") {
            alert("Input Field Is Empty..");
            return
        }
        if (EditModal) {
            dispatch(editTodo({ id: id, title: addTodoValue }));
            setIsModalVisible(false);
            return
        }
        const currentDate = moment().format('MMMM Do YYYY, h:mm a');
        dispatch(addTodo({ title: addTodoValue, date: currentDate }))
        setaddTodoValue("")
    }

    return (
        <div>
            <Modal visible={isModalVisible} className="AddTodoModal" width="800px" footer={null} centered closable={false} onCancel={handleCancel}>
                <div className="AddTodo">
                    <div className="input"><Input placeholder="Type Any Task..." onChange={(e) => setaddTodoValue(e.target.value)} value={addTodoValue} /></div>
                    <div className="buttons">
                        <Button type="primary" className="cancel" danger onClick={handleCancel}>Cancel</Button>
                        <Button type="primary" className="done" danger onClick={HandleAddtodo}>Done</Button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}
